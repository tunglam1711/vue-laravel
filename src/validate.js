export function validateBlog(blog) {
   const errors = [];
   //Required Title
   if (!blog.title || blog.title.length < 0) {
     errors.push("title");
   }
   //Required category
   if (!blog.category || blog.category.length < 0) {
     errors.push("category");
   }
   //Required position
   if (!blog.position || blog.position.length < 1) {
     errors.push("position");
   }
   //Required data_pubblic
   if (!blog.date_public || blog.date_public.length < 0) {
     errors.push("date_public");
   }
   //Required public
   if (!blog.is_published || blog.is_published.length < 0) {
     errors.push("is_published");
   }
   //Required des
   if (!blog.description || blog.description.length < 0) {
     errors.push("description");
   }
   //Required detail
   if (!blog.content || blog.content.length < 0) {
     errors.push("content");
   }
 
   return errors;
 }